#!/bin/bash

if [ -z $1 ]; then
	exit
fi

# Install all the printer profiles
if [ $1 == "--install" ]; then
	# Install GNOME-1.x printer files.
	if [ -d /usr/share/gnome-print ]; then
		cp -f /usr/share/gfax/fax-g3.profile /usr/share/gnome-print/profiles
	fi

	# Install GNOME-2.x printer files.
	for i in /usr/share/libgnomeprint/*
	do
		cp -f /usr/share/gfax/GFAX.xml $i/printers
		cp -f /usr/share/gfax/GNOME-GFAX-PS.xml $i/models
	done
fi


# Remove all the printer profiles
if [ $1 == "--remove" ]; then
	# Remove GNOME-1.x printer files.
	if [ -d /usr/share/gnome-print ]; then
		rm -f /usr/share/gnome-print/profiles/fax-g3.profile
	fi

	# Remove GNOME-2.x printer files.
	for i in /usr/share/libgnomeprint/*
	do
		rm -f $i/printers/GFAX.xml
		rm -f $i/models/GNOME-GFAX-PS.xml
	done
fi
