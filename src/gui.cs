//  GFAX - Gnome fax application
//  Copyright (C) 2003 George A. Farris
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

namespace gfax {
	using Mono.Posix;
	using System;
	using System.IO;
	using GLib;
	using Gtk;
	using Gnome;
	using Glade;
	using GConf.PropertyEditors;
	using GtkSharp;
	using System.Runtime.InteropServices;
	using System.Collections;
	using System.Reflection;

	
	//************************************************************************
	// Gfax class
	//
	// This is the main application window
	
	public class Gfax : Program
	{
		const string VERSION = "0.7.3";
		const string NAME = "Gfax";
		const string APPNAME = "Gfax";
		
		// Notebook pages
		const int SENDQ_PAGE = 0;
		const int DONEQ_PAGE = 1;
		const int RECEIVEQ_PAGE = 2;
		
		const int COLUMN_0 = 0;
		const int COLUMN_1 = 1;
		const int COLUMN_2 = 2;
		const int COLUMN_3 = 3;		
		const int ALL_COLUMNS = -1;	
	
		// Strings to hold old contact info temporarily
		string tempName;
		string tempNumber;
		string tempCompany;
		
		ArrayList oldSendQueue = null;		// hold old queue results
		
		bool restart = true;
		bool eventsEnabled = true;
		
		enum ActiveQ { done, send, receive };
		ActiveQ activeQ;
		
		
		Glade.XML gxml;
		
		[Glade.Widget] Gnome.AppBar Appbar;
						
		[Glade.Widget] Gtk.TreeView StatusList;
		[Glade.Widget] Gtk.TreeView JobsCompleteList;
		[Glade.Widget] Gtk.TreeView JobsReceivedList;		
		[Glade.Widget] Gtk.Notebook StatusNotebook;
		[Glade.Widget] Gtk.Button DeleteJobButton;
		[Glade.Widget] Gtk.TextView StatusText;
		
			
		// Menu items
		[Glade.Widget] Gtk.CheckMenuItem AutoQRefreshCheckMenuItem;
		[Glade.Widget] Gtk.CheckMenuItem EmailNotificationCheckMenuItem;
		[Glade.Widget] Gtk.CheckMenuItem HiResolutionModeCheckMenuItem;
		[Glade.Widget] Gtk.CheckMenuItem LogEnabledCheckMenuItem;

		Gtk.Window MainWindow;
				
		Gtk.ListStore StatusStore;
		Gtk.TreeModel StatusModel;
		G_ListView lv, jobsCompletedView, jobsReceivedView;
		
		// text buffer for status text area
		TextBuffer StatusTextBuffer = new TextBuffer(new TextTagTable());
		
		
		Phonebook[] myPhoneBooks;
		
		GConf.PropertyEditors.EditorShell shell;

		long filePos = 0;
		FileSystemWatcher watcher = null;
		
		static long lastMaxOffset;
		
		static FileStream myfile = new FileStream("/tmp/gfax.proc", 
					System.IO.FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
					 
		StreamReader reader = new StreamReader(myfile);

						
		public Gfax (string fname, string[] args)
			: base (APPNAME, VERSION, Modules.UI, args, new object [0])
		{
			Phonebook[] pb;
			
		
			Application.Init ();

			// check to see if we've run before, if so gfax will be there
			if ( Settings.RunSetupAtStart ) {
				Settings.RunSetupAtStart = false;
				
				MessageDialog md;
				md = new MessageDialog (
					null,
					DialogFlags.DestroyWithParent, 
					MessageType.Info, 
					ButtonsType.Ok,
					Catalog.GetString(
						@"
This is the first time you have run Gfax.
You should set your MODEM TYPE and PORT under preferences.

Gfax is initially setup to use Efax, you may change it use 
Hylafax if you prefer or require connection to a network 
facsimile server.")
				);
				md.Run ();
				md.Destroy();
			}
			
			if (!Directory.Exists(Defines.SPOOLDIR)) {
				G_Message gm = new G_Message(Catalog.GetString(
					@"Your spool directory is missing!
					
Please login as the root user and create the "
+ Defines.SPOOLDIR + 
" directory.\n\nAll users should be able to write to it.\n"));
				return;
			}
			if (!Directory.Exists(Defines.SPOOLDIR + "/doneq")) {
				G_Message gm = new G_Message(Catalog.GetString(
					@"The doneq directory is missing in your spool directory!
					
Please login as the root user and create the "
+ Defines.SPOOLDIR + "/doneq" +
" directory.\n\nAll users should be able to write to it.\n"));
				return;
			}
			if (!Directory.Exists(Defines.SPOOLDIR + "/recq")) {
				G_Message gm = new G_Message(Catalog.GetString(
					@"The recq directory is missing in your spool directory!
					
Please login as the root user and create the "
+ Defines.SPOOLDIR + "/recq" +
" directory.\n\nAll users should be able to write to it.\n"));
				return;
			}

			
			gxml = new Glade.XML (null, "gfax.glade", "Gfax", null);
			gxml.Autoconnect (this);


			// Set initial gui state as per preferences
			// GConf.PropertyEditors.EditorShell doesn't handle 
			// checkmenuitems;
			eventsEnabled = false;
		 		if (Settings.TransmitAgent == "hylafax") {
					AutoQRefreshCheckMenuItem.Active = Settings.RefreshQueueEnabled;
					EmailNotificationCheckMenuItem.Active = Settings.EmailNotify;
					HiResolutionModeCheckMenuItem.Active = Settings.HiResolution;
					LogEnabledCheckMenuItem.Active = Settings.LogEnabled;
				} 
				if (Settings.TransmitAgent == "efax") {
			 		AutoQRefreshCheckMenuItem.Active = Settings.RefreshQueueEnabled;
					AutoQRefreshCheckMenuItem.Sensitive = false;
					EmailNotificationCheckMenuItem.Active = false;
					EmailNotificationCheckMenuItem.Sensitive = false;
					HiResolutionModeCheckMenuItem.Active = Settings.HiResolution;
					LogEnabledCheckMenuItem.Visible = false;
				}		
			eventsEnabled = true;
			DeleteJobButton.Sensitive = false;
			StatusText.Editable = false;
			StatusText.CanFocus = false;
			StatusText.Buffer = StatusTextBuffer;	
			
			// Set the program icon
			Gdk.Pixbuf Icon = new Gdk.Pixbuf(null, "gfax.png");
			((Gtk.Window) gxml["Gfax"]).Icon = Icon;
			
			//if (Settings.TransmitAgent == "hylafax") {
				StatusStore = new ListStore(
							typeof (string),
							typeof (string),
							typeof (string),
							typeof (string),
							typeof (string),
							typeof (string),
							typeof (string),
							typeof (string));
			
				lv = new G_ListView(StatusList, StatusStore);
				lv.AddColumnTitle(Catalog.GetString("Jobid"), 0, 0);
				lv.AddColumnTitle(Catalog.GetString("Number"),0, 1);
				lv.AddColumnTitle(Catalog.GetString("Status"),0, 2);
				lv.AddColumnTitle(Catalog.GetString("Owner"),0, 3);
				lv.AddColumnTitle(Catalog.GetString("Pages"),0, 4);
				lv.AddColumnTitle(Catalog.GetString("Dials"),0, 5);
				lv.AddColumnTitle(Catalog.GetString("Send At"),0, 6);
				lv.AddColumnTitle(Catalog.GetString("Error"),0, 7);
				
				// List view for completed jobs tab
				jobsCompletedView = new G_ListView(JobsCompleteList, StatusStore);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Jobid"), 0, 0);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Number"),0, 1);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Status"),0, 2);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Owner"),0, 3);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Pages"),0, 4);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Dials"),0, 5);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Send At"),0, 6);
				jobsCompletedView.AddColumnTitle(Catalog.GetString("Error"),0, 7);

				jobsReceivedView = new G_ListView(JobsReceivedList, StatusStore);
				jobsReceivedView.AddColumnTitle(Catalog.GetString("Sender"), 0, 0);
				jobsReceivedView.AddColumnTitle(Catalog.GetString("Status"),0, 1);
				jobsReceivedView.AddColumnTitle(Catalog.GetString("Pages  "),0, 2);
				jobsReceivedView.AddColumnTitle(Catalog.GetString("Arrived"),0, 3);
				jobsReceivedView.AddColumnTitle(Catalog.GetString("Filename"),0, 4);
			
				StatusList.Selection.Changed += 
					new EventHandler(on_StatusList_selection);
				StatusList.Selection.Mode = SelectionMode.Multiple;				
				StatusList.HeadersVisible = true;
				
				JobsCompleteList.Selection.Changed += 
					new EventHandler(on_JobsCompleteList_selection);
				JobsCompleteList.Selection.Mode = SelectionMode.Multiple;				
				JobsCompleteList.HeadersVisible = true;
				
				JobsReceivedList.Selection.Changed += 
					new EventHandler(on_JobsReceivedList_selection);
				JobsReceivedList.Selection.Mode = SelectionMode.Multiple;				
				JobsReceivedList.HeadersVisible = true;

				// Make sure headers are visible
				lv.AddTextToRow("","","","","","","","");
				jobsCompletedView.AddTextToRow("","","","","","","","");
				jobsReceivedView.AddTextToRow("","","","","","","","");
				StatusStore.Clear();
			
				activeQ = ActiveQ.send;

				update_queue_status("sendq");
						
			
			if (Settings.RefreshQueueEnabled)
				Gtk.Timeout.Add((uint)(Settings.RefreshQueueInterval * 1000), 
					new Gtk.Function(queue_refresh));
			
			update_status(Fax.get_server_status());
		
			//start at the end of the file
    		lastMaxOffset = reader.BaseStream.Length;
			//ShowTail();
			watcher = new FileSystemWatcher(Path.GetDirectoryName("/tmp/gfax.proc"), Path.GetFileName("/tmp/gfax.proc"));
			watcher.NotifyFilter = NotifyFilters.LastWrite;
			watcher.Changed += new FileSystemEventHandler(FileUpdated);
			watcher.EnableRaisingEvents = true;
			
			Application.Run ();
		}


		public void ShowTail()
		{
   			if ( reader.BaseStream.Length == lastMaxOffset )
   			    return; 	//continue;

   			//seek to the last max offset
        	reader.BaseStream.Seek(lastMaxOffset, SeekOrigin.Begin);
			//read out of the file until the EOF
        	string line = "";
        	while ( (line = reader.ReadLine()) != null ) {
        		update_status(line);
			}
	   		//update the last max offset
    		lastMaxOffset = reader.BaseStream.Position;
   		}


		
		public void FileUpdated(object sender, FileSystemEventArgs args)
		{
			ShowTail();
		}          

		
		
		//************************************************************************
		// queue_refresh
		//		returns true to continue timeout functioning
		//	
		// Queue_refresh connects to the hylafax server and retrieves
		// updated queue information on regular intervals.  This is called
		// from a GLib.TimeoutHandler
		public bool queue_refresh ()
		{
			if (!Settings.RefreshQueueEnabled)
				return false;
			
			// always do main status			
			update_status(Fax.get_server_status());	
			// now do queue
			switch (activeQ) {
				case ActiveQ.done :
					update_queue_status("doneq");
					break;
				case ActiveQ.send :
					update_queue_status("sendq");
					break;
				case ActiveQ.receive :
					update_queue_status("recvq");
					break;
			}
			
			return true;	// must return true from timeout to keep it active
		}
		
		// Set to true if we need to restart for changed transport agent
		public bool Restart
		{
			get { return restart; }
			set { restart = value; }
		}
		
		

//========================= PRIVATE METHODS ====================================
		private void on_StatusList_selection (object o, EventArgs args)
		{
			DeleteJobButton.Sensitive = true;
		}

		private void on_JobsCompleteList_selection (object o, EventArgs args)
		{
			DeleteJobButton.Sensitive = true;
		}

		private void on_JobsReceivedList_selection (object o, EventArgs args)
		{
			DeleteJobButton.Sensitive = true;
		}

		private void on_exitButton_clicked (object o, EventArgs args)
		{
			Console.WriteLine("Goodbye...");
			Application.Quit();
		}

		private void on_mainWindow_delete_event (object o, DeleteEventArgs args) 
		{
			Application.Quit();
			args.RetVal = true;
		}

		private void on_exit1_activate (object o, EventArgs args)
		{
			Application.Quit();
			restart = true;
		}


//Status list notebook signal, switch pages to show different queues
		private void on_StatusNotebook_switch_page (object o, EventArgs args)
		{
			switch (StatusNotebook.CurrentPage) {
				case SENDQ_PAGE:
							activeQ = ActiveQ.send;
							if (update_queue_status("sendq") > 0)
								DeleteJobButton.Sensitive = true;
							else {
								DeleteJobButton.Sensitive = false;
							}
							break;
				case DONEQ_PAGE:
							DeleteJobButton.Sensitive = false;
							activeQ = ActiveQ.done;
							update_queue_status("doneq");
							break;
				case RECEIVEQ_PAGE:
							DeleteJobButton.Sensitive = false;
							activeQ = ActiveQ.receive;
							update_queue_status("recvq");
							break;
			}
		}

//Main toolbar buttons
//=======================================================================
		private void on_NewFaxButton_clicked (object o, EventArgs args)
		{
			send_new_fax();
		}
		

		private void on_PreferencesButton_clicked (object o, EventArgs args)
		{	
			GfaxPrefs gp = new GfaxPrefs();
		}


		private void on_PhonebookButton_clicked (object o, EventArgs args)
		{
			GfaxPhonebook gpb = new GfaxPhonebook();
		}


		private void on_Cancel_activate (object o, EventArgs args)
		{

		}
		
		private void on_DeleteJobButton_clicked (object o, EventArgs args)
		{
			// get the selected jobs
			ArrayList al;
			
			// get a list of jobids to kill
			al = lv.GetSelections(COLUMN_0);
			
			IEnumerator enu = al.GetEnumerator();
			while (	enu.MoveNext() ) {
				Fax.delete_job((string)enu.Current);
			}
			
			switch (activeQ) {
					case ActiveQ.done :
						update_queue_status("doneq");
						break;
					case ActiveQ.send :
						update_queue_status("sendq");
						break;
					case ActiveQ.receive :
						update_queue_status("recvq");
						break;
			}
			
			DeleteJobButton.Sensitive = false;
		}
		
		
// Menu items selected =============================================
		private void on_newfax_activate (object o, EventArgs args)
		{
			send_new_fax();	
		}

		private void on_jobs1_activate (object o, EventArgs args)
		{

		}

		private void on_auto_queue_refresh_check_menu_item_activate (object o, EventArgs args)
		{
			if (eventsEnabled) {
				Settings.RefreshQueueEnabled = !Settings.RefreshQueueEnabled;
				if (Settings.RefreshQueueEnabled)
					Gtk.Timeout.Add((uint)(Settings.RefreshQueueInterval * 1000), 
						new Gtk.Function(queue_refresh));
			}
		}

		private void on_log_enabled_check_menu_item_activate (object o, EventArgs args)
		{
			if (eventsEnabled)
				Settings.LogEnabled = !Settings.LogEnabled;
		}
		
		private void on_email_notification_check_menu_item_activate (object o, EventArgs args)
		{
			if (eventsEnabled)
				Settings.EmailNotify = !Settings.EmailNotify;
		}

		private void on_hi_resolution_mode_check_menu_item_activate (object o, EventArgs args)
		{
			if (eventsEnabled)
				Settings.HiResolution = !Settings.HiResolution;
		}

		private void on_preferences_activate (object o, EventArgs args)
		{
			GfaxPrefs gp = new GfaxPrefs();
		}
		
		private void on_about_activate (object o, EventArgs args)
		{
			Gdk.Pixbuf icon = new Gdk.Pixbuf (null, "gfax.png");
			string[] authors = new string[] { "George Farris <george@gmsys.com>" };
			string[] documentors = new string[] {};
			string copyright = Catalog.GetString("Copyright (C) 2003 George Farris <george@gmsys.com>");
			string description = Catalog.GetString("A Facsimile application for GNOME");
			string translators = ("Johannes Rohr <johannes@rohr.org> - German");

			
			G_About a = new G_About (NAME, VERSION, copyright,description, authors,
					documentors, translators, icon);
		}

		
//--------------------------- SUPPORT FUNCTIONS -------------------------------
		
		// Updates the status text widget
		private void update_status (string s)
		{
			if (s == null)
				return;
			
			if (Settings.TransmitAgent == "efax") {
				TextMark tm = StatusTextBuffer.GetMark("insert");				
				StatusTextBuffer.InsertAtCursor(String.Concat(s, "\n"));
				StatusText.ScrollMarkOnscreen(tm);
			} else {  // clear queue
				StatusTextBuffer.SetText(s);
			}
		}
		
		private int update_queue_status(string queue)
		{
			G_ListView view;
			Fax.FaxQueue q;
			Fax.FaxRecQueue rq;
						
			view = lv;
				
			switch (queue) {
				case "sendq":
						view = lv;
						break;
				case "doneq":
						view = jobsCompletedView;
						break;
				case "recvq":
						view = jobsReceivedView;
						break;
			}

			// update status bar
			Appbar.ClearStack();
			Appbar.Push(Catalog.GetString("Refreshing queue..."));
			Appbar.Refresh();
			//GLib.MainContext.Iteration ();
			while (Gtk.Application.EventsPending ())
               	Gtk.Application.RunIteration ();
					
			ArrayList reply = Fax.get_queue_status(queue);
						
			if (reply.Count > 0) {
				StatusStore.Clear();
				IEnumerator enu = reply.GetEnumerator();
				
				if (queue == "sendq" || queue == "doneq") {
					while ( enu.MoveNext() ) {
   	     				q = (Fax.FaxQueue)enu.Current;
						view.AddTextToRow(q.Jobid,q.Number,q.Status,q.Owner,q.Pages,q.Dials,q.Sendat,q.Error);
					}	
					
					Appbar.ClearStack();
						
					if (q.Jobid != "") {
						Appbar.Push(Catalog.GetString("There are " + reply.Count + " jobs in the queue"));
						Appbar.Refresh();
						((Gtk.Window) gxml["Gfax"]).Title = "Gfax (" + reply.Count + ")";
						return reply.Count;
					} else {
						Appbar.Push(Catalog.GetString("There are 0 jobs in the queue"));
						Appbar.Refresh();
						((Gtk.Window) gxml["Gfax"]).Title = "Gfax";
					}
				} else {  //recevive queue
					while ( enu.MoveNext() ) {
   	     				rq = (Fax.FaxRecQueue)enu.Current;
						view.AddTextToRow(rq.Sender, rq.Status, rq.Pages, rq.TimeReceived, rq.Filename);
					}
					
					if (rq.Sender != "") {
						Appbar.Push(Catalog.GetString("There are " + reply.Count + " jobs in the queue"));
						Appbar.Refresh();
						((Gtk.Window) gxml["Gfax"]).Title = "Gfax (" + reply.Count + ")";
						return reply.Count;
					} else {
						Appbar.Push(Catalog.GetString("There are 0 jobs in the queue"));
						Appbar.Refresh();
						((Gtk.Window) gxml["Gfax"]).Title = "Gfax";
					}
				}
				
				oldSendQueue = reply;	// else save queue
			}


			StatusStore.Clear();			
			return 0;
		}
	
		// This is where we end up if the New Fax button or the menu item 
		// has been selected.
		private void send_new_fax ()
		{
				
			string [] largs = {"do_filename"};
			GfaxSend sd = new GfaxSend ("", largs);
			
			// send the faxes
			if (sd.DoSend) {
				Fax.sendfax(sd.Filename);
				// if file is temp data (/var/spool/gfax/D.*) then delete it
				FileInfo f = new FileInfo(sd.Filename);
				if (File.Exists(Defines.SPOOLDIR + f.Name))
					File.Delete(Defines.SPOOLDIR + f.Name);
			}
			activeQ = ActiveQ.send;
			update_queue_status("sendq");
		}

	}


	//************************************************************************
    // NewPhoneBook class
    //
    // A new phone book druid, should make it easier for people to create
	// new phone books.  The old way was a little confusing.
    //
    public class NewPhoneBook
    {
		[Glade.Widget] Gtk.Dialog NewPhoneBookDialog;
		[Glade.Widget] Gnome.Druid NewPhoneBookDruid;
        [Glade.Widget] Gtk.RadioButton GfaxRadioButton;
        [Glade.Widget] Gtk.RadioButton EvolutionRadioButton;
        [Glade.Widget] Gtk.RadioButton DatabaseRadioButton;
        [Glade.Widget] Gtk.RadioButton LDAPRadioButton;
        [Glade.Widget] Gtk.Entry NewPhoneBookNameEntry;
 
		// Properties
		string phonebookname;
		string phonebooktype;
		
        public NewPhoneBook ()
        {
				Glade.XML xml = new Glade.XML (null, "gfax.glade","NewPhoneBookDialog",null);
                xml.Autoconnect (this);

                GfaxRadioButton.Active = true;
                 
                // turn these off until somewhere near supported
                DatabaseRadioButton.Visible = false;
                LDAPRadioButton.Visible = false;
								
				NewPhoneBookDruid.ShowAll();
        }
        
		public string PhoneBookName
		{
			get { return phonebookname; }
		}
		
		public string PhoneBookType
		{
			get { return phonebooktype; }
		}
		
        public void Run()
        {
            NewPhoneBookDialog.Run();
        }
              
        private void on_NewPhoneBookDialog_delete_event (object o, DeleteEventArgs args)
		{
			NewPhoneBookDialog.Hide();
			NewPhoneBookDialog.Dispose();
            args.RetVal = true;
        }

		private void on_NewPhoneBookDruidEdge_finish (object o, EventArgs args)
        {
            phonebookname = NewPhoneBookNameEntry.Text;
             
            if (GfaxRadioButton.Active)
                    phonebooktype = "gfax";
            else if (EvolutionRadioButton.Active)
                    phonebooktype = "evolution";
            else if (DatabaseRadioButton.Active)
                    phonebooktype = "sql";
            else if (LDAPRadioButton.Active)
                    phonebooktype = "ldap";

			NewPhoneBookDialog.Hide();
			NewPhoneBookDialog.Dispose();
        }

        private void on_NewPhoneBookDruid_cancel (object o, EventArgs args)
        {
			NewPhoneBookDialog.Hide();
			NewPhoneBookDialog.Dispose();
		}

    }



	//************************************************************************
	// GfaxSend class
	//
	// This is a the send window for sending of a facsimile.
		
	public class GfaxSend // : Program
	{
		const string APPNAME = "Gfax";
		
		const int COLUMN_0 = 0;
		const int COLUMN_1 = 1;
		const int COLUMN_2 = 2;
		const int COLUMN_3 = 3;
		const int ALL_COLUMNS = -1;	
		
		
		[Glade.Widget] Gtk.Button PhoneButton;
		[Glade.Widget] Gtk.Entry NumberEntry;
		[Glade.Widget] Gtk.Entry EmailEntry;
		[Glade.Widget] Gtk.Entry FilenameEntry;
		[Glade.Widget] Gtk.CheckButton ResolutionCheckbutton;
		[Glade.Widget] Gtk.CheckButton EmailCheckbutton;
		[Glade.Widget] Gtk.CheckButton SendCheckbutton;
		[Glade.Widget] Gtk.TreeView ItemTreeview;
		[Glade.Widget] Gnome.DateEdit SendDateedit;
		[Glade.Widget] Gnome.FileEntry PSFileEntry;


		Gtk.ListStore ItemStore;
		G_ListView ItemListview;
		
		GConf.PropertyEditors.EditorShell shell;		
		
		Glade.XML gxml;
		string filename, includeFilename;
		bool dosend;
		
		public GfaxSend (string fname, string[] args)
		{
			
			filename = fname;
			includeFilename = args[0];
			
			Application.Init ();
			
			// check to see if we've run before, if so gfax will be there
			if ( Settings.RunSetupAtStart ) {
				Settings.RunSetupAtStart = false;
				MessageDialog md;
				md = new MessageDialog (
					null,
					DialogFlags.DestroyWithParent, 
					MessageType.Info, 
					ButtonsType.Ok,
					Catalog.GetString(
						@"
This is the first time you have run Gfax.
Please run Gfax from the menu or command line and set your 
MODEM TYPE and PORT under preferences.

Gfax is initially setup to use Efax, you may change it use 
Hylafax if you prefer or require connection to a network 
facsimile server.")
				);
				md.Run ();
				md.Destroy();
				Application.Quit();
				dosend = false;
				Environment.Exit(0);
			}

			gxml = new Glade.XML (null, "send-druid.glade","NewFaxDialog",null);
			gxml.Autoconnect (this);
						
			// Set the program icon
			Gdk.Pixbuf Icon = new Gdk.Pixbuf(null, "gfax.png");
			((Gtk.Window) gxml["NewFaxDialog"]).Icon = Icon;
			
			
			ItemStore = new ListStore(
					typeof (Boolean),
					typeof (string),
					typeof (string),
					typeof (string));
			
			ItemListview = new G_ListView(ItemTreeview, ItemStore);
			
			ItemListview.AddColumnTitleToggle(Catalog.GetString("Send"), 0, COLUMN_0);
			ItemListview.AddColumnTitle(Catalog.GetString("Phone Number"), 0, COLUMN_1);
			ItemListview.AddColumnTitle(Catalog.GetString("Organization"), 0, COLUMN_2);
			ItemListview.AddColumnTitle(Catalog.GetString("Contact"), 0, COLUMN_3);
			
			ItemTreeview.HeadersVisible = true;

			//ItemTreeview.Selection.Mode = SelectionMode.Multiple;

			ResolutionCheckbutton.Active = Settings.HiResolution;
			EmailCheckbutton.Active = Settings.EmailNotify;
			EmailEntry.Text = Settings.EmailAddress;
			
			if (Settings.TransmitAgent == "efax") {
				EmailCheckbutton.Visible = false;
				EmailEntry.Visible = false;
				SendCheckbutton.Visible = false;
				SendDateedit.Visible = false;
			}

			if (SendCheckbutton.Active)
				SendDateedit.Sensitive = false;
				
			// If we have a file name from the gnome print dialog
			if (includeFilename != "do_filename") {
				FilenameEntry.Text = Catalog.GetString("Spooled from print job");
				PSFileEntry.Sensitive = false;
			}
			Application.Run ();
		}

		public bool DoSend
		{
			get { return dosend; }
		}

		public string Filename
		{
			get { return filename; }
		}


		private void on_window1_delete_event (object o, DeleteEventArgs args) 
		{
			dosend = false;		
			((Gtk.Window) gxml["NewFaxDialog"]).Destroy();			
			Application.Quit ();
			args.RetVal = true;
		}
		
		private void on_CancelButton_clicked (object o, EventArgs args) 
		{
			dosend = false;
			((Gtk.Window) gxml["NewFaxDialog"]).Destroy();
			Application.Quit ();
		}

		private void on_NumberEntry_changed (object o, EventArgs args) 
		{
			//if (NumberEntry.Text == "\t")
				//((Gnome.Druid) gxml["send_druid"]).HasFocus = true;
		}
		
		private void on_SendCheckButton_toggled (object o, EventArgs args)
		{
			SendDateedit.Sensitive = ! SendDateedit.Sensitive;
		}
		
		private void on_NumberEntry_activate (object o, EventArgs args) 
		{
			Gtk.TreeIter iter = new Gtk.TreeIter();
			
			iter = ItemStore.AppendValues(true, NumberEntry.Text, "", "");
			ItemTreeview.Model = ItemStore;
			NumberEntry.Text = "";
		}
		

		private void  on_PhoneButton_clicked (object o, EventArgs args) 
		{
			Gtk.TreeIter iter = new Gtk.TreeIter();
			// display the phone book, when we return gfax.Destinations 
			// will be set with contacts.
			GfaxSendPhoneBook gfs = new GfaxSendPhoneBook(gxml, "gfaxsend");
			
			ItemStore.Clear();

			if (gfax.Destinations.Count > 0) {
				IEnumerator enu = gfax.Destinations.GetEnumerator();
    	  		
				while ( enu.MoveNext() ) {
					iter = ItemStore.AppendValues(true,
							((Contact)enu.Current).PhoneNumber,
							((Contact)enu.Current).Organization,
							((Contact)enu.Current).ContactPerson);
					ItemTreeview.Model = ItemStore;
				}
			}
		}


		// When finished gfax.Destinations will contain a list of contacts
		// of Phonebook.Contact type.
		private void on_SendfaxButton_clicked (object o, EventArgs args) 
		{	
			Gtk.TreeIter iter = new Gtk.TreeIter();
			ArrayList rows = new ArrayList();
			
			if (includeFilename == "do_filename")
				filename = FilenameEntry.Text;
				
			// clear all the distinations, it's a little wierd yup
			gfax.Destinations.Clear();
			
			// Get the first row.
			ItemStore.GetIterFirst(out iter);

			try {
				if ( (bool)ItemStore.GetValue(iter, 0) ) {		// if send is true (toggle set)
										
					Contact c = new Contact();
					c.PhoneNumber = (string)ItemStore.GetValue(iter, 1);	// number
					c.Organization = (string)ItemStore.GetValue(iter, 2);	// organization
					c.ContactPerson = (string)ItemStore.GetValue(iter, 3);	// contact
										
					rows.Add(c);
				}
			}
			catch (Exception e) {}
			
			// get the rest of the rows			
			while (ItemStore.IterNext(ref iter)) {

				try {
					if ( (bool)ItemStore.GetValue(iter, 0) ) {		// if send is true (toggle set)
										
						Contact c = new Contact();
						c.PhoneNumber = (string)ItemStore.GetValue(iter, 1);	// number
						c.Organization = (string)ItemStore.GetValue(iter, 2);	// organization
						c.ContactPerson = (string)ItemStore.GetValue(iter, 3);	// contact
										
						rows.Add(c);
					}
				}
				catch (Exception e) {}
			}


			if (!SendCheckbutton.Active) {
				// Convert to UTC for Hylafax
				gfax.timeToSend = (SendDateedit.Time).ToUniversalTime();
			}

			gfax.Destinations = rows;
			
			//get the fine resolution status
			gfax.sendWizardResolution = ResolutionCheckbutton.Active;
			//get the email flag and email address
			gfax.sendWizardEmailNotify = EmailCheckbutton.Active;
			gfax.sendWizardEmailAddress = EmailEntry.Text;				
			
			if ( gfax.Destinations.Count > 0 ) 
				dosend = true;	// yes send the fax
				
			((Gtk.Window) gxml["NewFaxDialog"]).Hide();
			Application.Quit();
		}

	}

	
	
	
	//************************************************************************
	// GfaxPrefs class
	//
	// This is the preferences window.
	public class GfaxPrefs
	{
		Glade.XML gxml;
		[Glade.Widget] Gtk.Dialog PrefsDialog;
		[Glade.Widget] Gtk.Notebook PrefsNotebook;
		// System Tab
		[Glade.Widget] Gtk.RadioButton HylafaxRadioButton;
		[Glade.Widget] Gtk.RadioButton MgettyRadioButton;
		[Glade.Widget] Gtk.RadioButton EfaxRadioButton;
		[Glade.Widget] Gtk.Entry FaxNumberEntry;
		[Glade.Widget] Gtk.Entry DialPrefixEntry;
		[Glade.Widget] Gtk.Entry ModemEntry;
		
		//Hylafax Tab
		[Glade.Widget] Gtk.Entry HylafaxHostnameEntry;
		[Glade.Widget] Gtk.Entry HylafaxPortEntry;
		[Glade.Widget] Gtk.Entry HylafaxUsernameEntry;
		
		//Efax Tab
		[Glade.Widget] Gtk.Entry EfaxModemDeviceEntry;
		[Glade.Widget] Gtk.OptionMenu EfaxPapersizeOptionMenu;
		[Glade.Widget] Gtk.OptionMenu EfaxModemTypeOptionMenu;
		[Glade.Widget] Gtk.OptionMenu EfaxModemSpeakerVolumeOptionMenu;
		
		//System Tab
		[Glade.Widget] Gtk.CheckButton EmailNotifyCheckButton;
		[Glade.Widget] Gtk.Entry	EmailAddressEntry;
		[Glade.Widget] Gtk.CheckButton SendNowCheckButton;
		[Glade.Widget] Gtk.CheckButton FaxLogCheckButton;
		[Glade.Widget] Gtk.CheckButton CoverPageCheckButton;
		[Glade.Widget] Gtk.CheckButton HiResCheckButton;

		// we set this otherwise setting inital values will wipe
		// out all the other settings.
		bool eventsEnabled = false; 

		bool taChanged = false;
		
		const int HYLAFAX_PAGE = 1;
		const int MGETTY_PAGE = 2;
		const int EFAX_PAGE = 3;
		
		string[] papersize = {"letter", "legal", "a4"};
		
		string[] modemType = {@"-j\Q4",@"-j\Q1",@"-j*F1",@"-j&H2&I0&R1&D3I4",@"-or"};
		/*
		# FCINIT='-j\Q4'                # AT&T (Dataport, Paradyne)
		# FCINIT='-j\Q1'                # Motorola (Power Modem, 3400 Pro,...)
		# FCINIT='-j*F1'                # QuickComm (Spirit II)
		# FCINIT='-j&H2&I0&R1&D3I4'     # USR (Courier, Sportster)
		# FCINIT='-or'                  # Multi-Tech (for bit reversal)
		*/
		
		
		public GfaxPrefs ()
		{
			gxml = new Glade.XML (null, "gfax.glade","PrefsDialog",null);
			//GConf.PropertEditors
			EditorShell shell = new EditorShell (gxml);
			gxml.Autoconnect(this);
			
			
			// System Tab
			if (Settings.TransmitAgent == "hylafax") {
				HylafaxRadioButton.Active = true;
				CoverPageCheckButton.Visible = false;
				PrefsNotebook.GetNthPage(MGETTY_PAGE).Hide();
				PrefsNotebook.GetNthPage(EFAX_PAGE).Hide();
			}
			else if (Settings.TransmitAgent == "mgetty") {
				MgettyRadioButton.Active = true;
				PrefsNotebook.GetNthPage(HYLAFAX_PAGE).Hide();
				PrefsNotebook.GetNthPage(EFAX_PAGE).Hide();
			}
			else if (Settings.TransmitAgent == "efax") {
				EfaxRadioButton.Active = true;
				EmailNotifyCheckButton.Sensitive = false;
				EmailAddressEntry.Sensitive = false;
				Settings.SendNow = SendNowCheckButton.Active;
				FaxLogCheckButton.Visible = false;
				CoverPageCheckButton.Visible = false;
				Settings.HiResolution = HiResCheckButton.Active;

				PrefsNotebook.GetNthPage(HYLAFAX_PAGE).Hide();
				PrefsNotebook.GetNthPage(MGETTY_PAGE).Hide();

			 	switch (Settings.EfaxPapersize) {
					case "letter":
						EfaxPapersizeOptionMenu.SetHistory(0);
						break;
					case "legal":
						EfaxPapersizeOptionMenu.SetHistory(1);
						break;
					case "a4":
						EfaxPapersizeOptionMenu.SetHistory(2);
						break;
					default:
						EfaxPapersizeOptionMenu.SetHistory(0);
						break;
				}
				//"-j\\Q4","-j\\Q1","-j*F1","-j&H2&I0&R1&D3I4","-or"
				switch (Settings.EfaxModemFcinit) {
					case @"-j\Q4":
						EfaxModemTypeOptionMenu.SetHistory(0);
						break;
					case @"-j\Q1":
						EfaxModemTypeOptionMenu.SetHistory(1);
						break;
					case @"-j*F1":
						EfaxModemTypeOptionMenu.SetHistory(2);
						break;
					case @"-j&H2&I0&R1&D3I4":
						EfaxModemTypeOptionMenu.SetHistory(3);
						break;
					case @"-or":
						EfaxModemTypeOptionMenu.SetHistory(4);
						break;
					default:
						EfaxModemTypeOptionMenu.SetHistory(5);
						break;
				}
								
				EfaxModemSpeakerVolumeOptionMenu.SetHistory((uint)Settings.EfaxModemSpeakerVolume);
			}
			
			
			// changes that happen automagically
			shell.Add(SettingKeys.FaxNumber, "FaxNumberEntry");
			shell.Add(SettingKeys.PhonePrefix, "DialPrefixEntry");
			
			// Hylafax Tab
			shell.Add(SettingKeys.Hostname, "HylafaxHostnameEntry");
			shell.Add(SettingKeys.Port, "HylafaxPortEntry");
			shell.Add(SettingKeys.Username, "HylafaxUsernameEntry");
			
			// Mgetty Tab
			
			// Efax Tab
			shell.Add(SettingKeys.EfaxModemDevice, "EfaxModemDeviceEntry");
			
			
			// User tab
			shell.Add(SettingKeys.EmailNotify, "EmailNotifyCheckButton");
			shell.Add(SettingKeys.EmailAddress, "EmailAddressEntry");
			shell.Add(SettingKeys.SendNow, "SendNowCheckButton");
			shell.Add(SettingKeys.LogEnabled, "FaxLogCheckButton");
			shell.Add(SettingKeys.CoverPage, "CoverPageCheckButton");
			shell.Add(SettingKeys.HiResolution, "HiResCheckButton");

			eventsEnabled = true;

		}
		
	
		private void on_PrefsDialog_delete_event (object o, DeleteEventArgs args) 
		{
			MessageDialog md;
			if (taChanged) {
				md = new MessageDialog (
					null,
					DialogFlags.DestroyWithParent, 
					MessageType.Warning, 
					ButtonsType.Ok,
					Catalog.GetString("You need to exit Gfax and restart it when you change transport agents")
				);
				md.Run ();
				md.Destroy();
			}					
			PrefsDialog.Destroy();
			args.RetVal = true;
		}

		private void on_CloseButton_clicked (object o, EventArgs args) 
		{	
			MessageDialog md;
			if (taChanged) {
				md = new MessageDialog (
					null,
					DialogFlags.DestroyWithParent, 
					MessageType.Warning, 
					ButtonsType.Ok,
					Catalog.GetString("You need to exit Gfax and restart it when you change transport agents")
				);
				md.Run ();
				md.Destroy();
			}					
			PrefsDialog.Destroy();
		}
		private void on_HelpButton_clicked (object o, EventArgs args) 
		{
			
		}
		
		private void system_tab_changed (object o, EventArgs args) 
		{
			if (eventsEnabled) {
				//eventsEnabled = false;
				if (HylafaxRadioButton.Active) {
					Settings.TransmitAgent = "hylafax";
					Settings.RefreshQueueInterval = 60;
					Settings.RefreshQueueEnabled = false;
					PrefsNotebook.GetNthPage(HYLAFAX_PAGE).Show();
					PrefsNotebook.GetNthPage(MGETTY_PAGE).Hide();
					PrefsNotebook.GetNthPage(EFAX_PAGE).Hide();
				}					
				else if (MgettyRadioButton.Active) {
					Settings.TransmitAgent = "mgetty";
					PrefsNotebook.GetNthPage(MGETTY_PAGE).Show();
					PrefsNotebook.GetNthPage(HYLAFAX_PAGE).Hide();
					PrefsNotebook.GetNthPage(EFAX_PAGE).Hide();
				}
				else if (EfaxRadioButton.Active) {
					Settings.TransmitAgent = "efax";
					Settings.RefreshQueueInterval = 15;
					Settings.RefreshQueueEnabled = true;
					PrefsNotebook.GetNthPage(EFAX_PAGE).Show();
					PrefsNotebook.GetNthPage(HYLAFAX_PAGE).Hide();
					PrefsNotebook.GetNthPage(MGETTY_PAGE).Hide();
				}

				Settings.FaxNumber = FaxNumberEntry.Text;
				Settings.PhonePrefix = DialPrefixEntry.Text;
			}
		}
		
		private void user_tab_changed (object o, EventArgs args) 
		{
			if (eventsEnabled) {
				Settings.EmailNotify = EmailNotifyCheckButton.Active;
				Settings.EmailAddress = EmailAddressEntry.Text;
				Settings.SendNow = SendNowCheckButton.Active;
				Settings.LogEnabled = FaxLogCheckButton.Active;
				Settings.CoverPage = CoverPageCheckButton.Active;
				Settings.HiResolution = HiResCheckButton.Active;
			}
		}
		
		private void radio_button_toggled (object o, EventArgs args) 
		{	
			if (eventsEnabled)
				taChanged = true;
		}

		private void mgetty_setup_changed (object o, EventArgs args) 
		{
		}
		
		private void efax_setup_changed (object o, EventArgs args) 
		{	
		
			if (eventsEnabled) {
				// Option 5 is custom define so we don't change anything
				if (EfaxModemTypeOptionMenu.History != 5)
					Settings.EfaxModemFcinit = modemType[EfaxModemTypeOptionMenu.History];
				Settings.EfaxPapersize = papersize[EfaxPapersizeOptionMenu.History];
				Settings.EfaxModemSpeakerVolume = EfaxModemSpeakerVolumeOptionMenu.History;
			}
		}
	}

	
	
	//************************************************************************
	// GfaxPhonebook class
	//
	
	public class GfaxPhonebook
	{
		const string APPNAME = "Gfax";
		
		const int COLUMN_0 = 0;
		const int COLUMN_1 = 1;
		const int COLUMN_2 = 2;
		const int COLUMN_3 = 3;
		const int ALL_COLUMNS = -1;
		
		string tempName;
		string tempNumber;
		string tempCompany;
		
		bool eventsEnabled = true;
		
		[Glade.Widget] Gtk.Combo PhonebookCombo;
		[Glade.Widget] Gtk.TreeView ItemTreeview;
		[Glade.Widget] Gtk.Entry PhonebookComboEntry;
		[Glade.Widget] Gtk.Entry EditPhbNumberEntry;
		[Glade.Widget] Gtk.Entry EditPhbNameEntry;
		[Glade.Widget] Gtk.Entry EditPhbCompanyEntry;
		[Glade.Widget] Gtk.Button SaveCloseButton;
		[Glade.Widget] Gtk.Button AddButton;
		[Glade.Widget] Gtk.Button UpdateButton;		
		[Glade.Widget] Gtk.Button ClearButton;
		[Glade.Widget] Gtk.Statusbar Statusbar;
		
		Gtk.ListStore ItemStore;	//phonebook item store
		G_ListView ItemView;		
		
		GConf.PropertyEditors.EditorShell shell;		
		Phonebook[] myPhoneBooks;		
		Glade.XML gxml;
		const int id = 1;
		
		public GfaxPhonebook ()
		{

			myPhoneBooks = Phonetools.get_phonebooks();
						
			Application.Init ();
			gxml = new Glade.XML (null, "send-druid.glade","PhbookWindow",null);
			gxml.Autoconnect (this);

			//Set the program icon
			Gdk.Pixbuf Icon = new Gdk.Pixbuf(null, "gfax.png");
			((Gtk.Window) gxml["PhbookWindow"]).Icon = Icon;
			
			ItemStore = new ListStore(typeof (string), typeof (string), typeof (string));
			ItemView = new G_ListView(ItemTreeview, ItemStore);
			ItemView.AddColumnTitle(Catalog.GetString("Organization"), 0, COLUMN_0);
			ItemView.AddColumnTitle(Catalog.GetString("Phone Number"), 0, COLUMN_1);
			ItemView.AddColumnTitle(Catalog.GetString("Contact"), 0, COLUMN_2);
			ItemTreeview.HeadersVisible = true;
			//ItemTreeview.Selection.Mode = SelectionMode.Multiple;
			ItemTreeview.Selection.Changed += 
					new EventHandler(on_ItemTreeview_selection);
			
			// Populate the drop down combo box with phonebooks and populate
			// the list with the first phonebook.
			// TODO sort these alphabetically
			if ( myPhoneBooks.Length > 0) {
				string[] list = new string[myPhoneBooks.Length];
				
				if ( myPhoneBooks != null ) {
					// populate the list
					int i = 0;
					foreach (Phonebook p in myPhoneBooks)
						list[i++] = p.Name;
				}

				PhonebookCombo.PopdownStrings = list;
			}
			
			SaveCloseButton.Sensitive = false;
			UpdateButton.Sensitive = false;
			ClearButton.Sensitive = false;
			AddButton.Sensitive = false;
			
			Application.Run ();
		}


		private void on_PhbookWindow_delete_event (object o, DeleteEventArgs args) 
		{
			((Gtk.Window) gxml["PhbookWindow"]).Destroy();			
			Application.Quit ();
		}
		
		// Menu signals
		private void on_New_activate (object o, EventArgs args)
		{
		}
		
		private void on_Save_activate (object o, EventArgs args)
		{
		}
		
		private void on_SaveAs_activate (object o, EventArgs args)
		{
		}
		
		private void on_SaveAndClose_activate (object o, EventArgs args)
		{
		}
		
		private void on_Delete_activate (object o, EventArgs args)
		{
		}
		
		private void on_Close_activate (object o, EventArgs args)
		{
		}
		//--------- end of menu -----------------

		
		private void on_NewPhonebookButton_clicked (object o, EventArgs args)
		{
			// Unselect any phone book otherwise we get gtk errors.
			ItemTreeview.Selection.UnselectAll();
			
			// run the wizard
			NewPhoneBook nphb = new NewPhoneBook ();
            nphb.Run();

			if ( nphb.PhoneBookName != null )	{	// don't do this if cancelled
				Phonebook ph = new Phonebook();
			
				// TODO  get location from gconf
				string HOMEDIR = Environment.GetEnvironmentVariable("HOME");			
				
				ph.Path = HOMEDIR + "/.etc/gfax/" + nphb.PhoneBookName;
				ph.Name = nphb.PhoneBookName;	// get the new book name
				ph.Type = nphb.PhoneBookType;
				Phonetools.add_book(ph);
			
				// Reload the phone books
				myPhoneBooks = Phonetools.get_phonebooks();
				if ( myPhoneBooks.Length > 0) {
					string[] list = new string[myPhoneBooks.Length];
					
					if ( myPhoneBooks != null ) {
						// populate the list
						int i = 0;
						foreach (Phonebook p in myPhoneBooks)
							list[i++] = p.Name;
					}
					PhonebookCombo.PopdownStrings = list;
				}
			}
        }

		private void on_DeletePhonebookButton_clicked (object o, EventArgs args)
		{
			string book = PhonebookComboEntry.Text;
			
			MessageDialog md = new MessageDialog (
					null,
					DialogFlags.DestroyWithParent, 
					MessageType.Question, 
					ButtonsType.YesNo,
					Catalog.GetString("Are you sure you want to delete the phone book?")
			);
     
			ResponseType result = (ResponseType)md.Run ();

			if (result == ResponseType.Yes) {
				md.Destroy();
				
				if (book == null)
					return;
				Phonetools.delete_book(book); 
				// now reload
				myPhoneBooks = Phonetools.get_phonebooks();
				ItemStore.Clear();
				if ( myPhoneBooks.Length > 0) {
					string[] list = new string[myPhoneBooks.Length];
					
					if ( myPhoneBooks != null ) {
						// populate the list
						int i = 0;
						foreach (Phonebook p in myPhoneBooks)
							list[i++] = p.Name;
					}
					PhonebookCombo.PopdownStrings = list;
				}
			} else {
				md.Destroy();
			}
		}
	
		private void on_SaveCloseButton_clicked (object o, EventArgs args)
		{
			ArrayList rows;
			ArrayList contacts = new ArrayList();
			string book;
			
			book = PhonebookComboEntry.Text;
			
			// just get all items and save
			SaveCloseButton.Sensitive = false;
			//eventsEnabled = false;
			EditPhbCompanyEntry.Text = "";
			EditPhbNumberEntry.Text = "";
			EditPhbNameEntry.Text = "";
			
			rows = ItemView.GetAllRows();
						
			IEnumerator enu = rows.GetEnumerator();
    	  	while ( enu.MoveNext() ) {
				Contact c = new Contact();
				c.Organization = ((string[])enu.Current)[0];
				c.PhoneNumber = ((string[])enu.Current)[1];
				c.ContactPerson = ((string[])enu.Current)[2];
				contacts.Add(c);
			}
			
			Phonetools.save_phonebook_items(book, contacts);
			//EditPhbList.Selection.UnselectAll();
			
			((Gtk.Window) gxml["PhbookWindow"]).Destroy();			
			Application.Quit ();
		}
		
		private void on_CloseButton_clicked (object o, EventArgs args)
		{
			if (SaveCloseButton.Sensitive) {
				MessageDialog md = new MessageDialog (
					null,
					DialogFlags.DestroyWithParent, 
					MessageType.Question, 
					ButtonsType.YesNo,
					Catalog.GetString("You have unsaved phone book entries.\n Are you sure you want to Quit?")
				);
     
				ResponseType result = (ResponseType)md.Run ();

				if (result == ResponseType.Yes) {
					md.Destroy();
					((Gtk.Window) gxml["PhbookWindow"]).Destroy();			
					Application.Quit ();
				} else {
					md.Destroy();
				}
			} else {
				((Gtk.Window) gxml["PhbookWindow"]).Destroy();			
				Application.Quit ();
			}
		}


		private void on_PhonebookComboEntry_insert_text (object o, EventArgs args)
		{
				ArrayList contacts = null;
				string name, number, company;
				
				// get the first book in the list and load the liststore
				Phonebook p = Phonetools.get_book_from_name(PhonebookComboEntry.Text);

				// Clear the list_store
				ItemStore.Clear();
				
				contacts = Phonetools.get_contacts(p);
				
				if (contacts != null) {
					IEnumerator enu = contacts.GetEnumerator();
					while ( enu.MoveNext() ) {
						Contact c = new Contact();
						c = (Contact)enu.Current;
						ItemView.AddTextToRow(c.Organization, c.PhoneNumber, c.ContactPerson);
					}
				}
		}

		private void on_EditPhbNumberEntry_changed (object o, EventArgs args)
		{
			if (eventsEnabled) {
				ArrayList al = ItemView.GetSelections(ALL_COLUMNS);
				// if single contact selected
				if (al.Count > 0) {
					UpdateButton.Sensitive = true;
				} else {
					AddButton.Sensitive = true;
					ClearButton.Sensitive = true;
				}
			}
		}
		private void on_EditPhbNameEntry_changed (object o, EventArgs args)
		{
			if (eventsEnabled) {
				ArrayList al = ItemView.GetSelections(ALL_COLUMNS);
				// if single contact selected
				if (al.Count > 0) {
					UpdateButton.Sensitive = true;
				} else {
					AddButton.Sensitive = true;
					ClearButton.Sensitive = true;
				}
			}
		}
		private void on_EditPhbCompanyEntry_changed (object o, EventArgs args)
		{
			if (eventsEnabled) {
				ArrayList al = ItemView.GetSelections(ALL_COLUMNS);
				// if single contact selected
				if (al.Count > 0) {
					UpdateButton.Sensitive = true;
				} else {
					AddButton.Sensitive = true;
					ClearButton.Sensitive = true;
				}
			}
		}
		
		private void on_ClearButton_clicked (object o, EventArgs args)
		{
			eventsEnabled = false;
			EditPhbCompanyEntry.Text = "";
			EditPhbNumberEntry.Text = "";
			EditPhbNameEntry.Text = "";
			UpdateButton.Sensitive = false;
			ItemTreeview.Selection.UnselectAll();
			ClearButton.Sensitive = false;
			AddButton.Sensitive = false;
			Statusbar.Pop(id);
			Statusbar.Push(id, " ");
		}
		
		private void on_AddButton_clicked (object o, EventArgs args)
		{
			ItemView.AddTextToRow(EditPhbCompanyEntry.Text,
					EditPhbNumberEntry.Text,
					EditPhbNameEntry.Text);
					
			eventsEnabled = false;  // no updates till selected
			EditPhbCompanyEntry.Text = "";
			EditPhbNumberEntry.Text = "";
			EditPhbNameEntry.Text = "";
			EditPhbNumberEntry.HasFocus = true;
			SaveCloseButton.Sensitive = true;
			ClearButton.Sensitive = false;
			AddButton.Sensitive = false;
		}

		private void on_UpdateButton_clicked (object o, EventArgs args)
		{
			ItemView.UpdateColumnText(tempNumber, EditPhbNumberEntry.Text, COLUMN_1);
			ItemView.UpdateColumnText(tempName, EditPhbNameEntry.Text, COLUMN_2);
			ItemView.UpdateColumnText(tempCompany, EditPhbCompanyEntry.Text, COLUMN_0);
			EditPhbCompanyEntry.Text = "";
			EditPhbNumberEntry.Text = "";
			EditPhbNameEntry.Text = "";
			UpdateButton.Sensitive = false;
			ItemTreeview.Selection.UnselectAll();
			ClearButton.Sensitive = false;
			SaveCloseButton.Sensitive = true;
			Statusbar.Pop(id);
			Statusbar.Push(id, " ");
		}
		
		private void on_ItemTreeview_selection (object o, EventArgs args)
		{	
			ArrayList al;
			
			eventsEnabled = false;  // no updates			
			al = ItemView.GetSelections(ALL_COLUMNS);
			Statusbar.Push(id,Catalog.GetString("Press the <DELETE> key to delete an entry."));
			
			// if single contact selected
			if (al.Count == 3) {
				IEnumerator enu = al.GetEnumerator();
				enu.MoveNext();
				EditPhbCompanyEntry.Text = (string)enu.Current;
				tempCompany = (string)enu.Current;
				enu.MoveNext();
				EditPhbNumberEntry.Text = (string)enu.Current;
				tempNumber = (string)enu.Current;
				enu.MoveNext();
				EditPhbNameEntry.Text = (string)enu.Current;
				tempName = (string)enu.Current;
			}
			AddButton.Sensitive = false;
			ClearButton.Sensitive = true;
			eventsEnabled = true;
		}

		private void on_ItemTreeview_key_press_event (object o, KeyPressEventArgs args)
		{
			if (args.Event.Key == Gdk.Key.Delete ) {
						
				//eventsEnabled = false;  // no updates			
				ArrayList al = ItemView.GetSelections(ALL_COLUMNS);
			
				// if single contact selected
				//if (al.Count == 3) {
					ItemView.RemoveSelectedRow();
					EditPhbCompanyEntry.Text = "";
					EditPhbNumberEntry.Text = "";
					EditPhbNameEntry.Text = "";
				//}
				AddButton.Sensitive = false;
				ClearButton.Sensitive = false;
				SaveCloseButton.Sensitive = true;
				//eventsEnabled = true;
			}
		}
		
		
	}
}
