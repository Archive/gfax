//  GFAX - Gnome fax application
//  Copyright (C) 2003 George A. Farris
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

namespace gfax {
	using Mono.Posix;
	using System;
	using System.IO;
	using System.Collections;
	
	public class gfax
	{	
		// Gfax Global variables ( ya, ya I know)
		//
		// list of contacts of type Contact to xmit, we use this everywhere so it is
		// simpler to make it public and global.
		public static ArrayList Destinations = new ArrayList();
		public static DateTime timeToSend;
		public static bool fromSendWizard = false;
		public static bool sendWizardResolution = false;
		public static bool sendWizardEmailNotify = false;
		public static string sendWizardEmailAddress = null;
		public static string hylafaxPassword = null;
		public static Efax efax;

		const bool TOPLEVEL = true;
				
		public static void Main (string[] args)
		{
		
			const string GCONFDIR = "/.gconf/apps/gfax";
			
			string HOMEDIR = Environment.GetEnvironmentVariable("HOME");
			string filename = null;	
			
			// Initialize GETTEXT
			Catalog.Init ("gfax", Defines.GNOME_LOCALE_DIR);

			// Touch the /tmp/gfax.proc file, we want it to exist
			try {
				StreamWriter sw = File.CreateText("/tmp/gfax.proc");
				sw.Close();
			} catch (Exception e) {
				Console.WriteLine(Catalog.GetString("Can't create /tmp/gfax.proc"));
				Environment.Exit(1);
			};

			
			// handle command line args ourselves					
			for (int i=0; i < args.Length; i++ ) {
				//Console.WriteLine("{0} {1}", i, args[i]);
				switch (args[i])
				{
					case "--help" :
								Console.WriteLine (Catalog.GetString("Gfax help..."));
								Console.WriteLine ("Gfax spool dir -> {0}", Defines.SPOOLDIR);
								break;
					case "-f" :
								filename = args[i+1];
								break;
					default:
								if (File.Exists(args[i]))
									filename = args[i];
								break;
				}
			}
			
			try {
				if ( Settings.RunSetupAtStart ) {
					// Set some default preferences.
					Settings.TransmitAgent = "efax";
					Settings.SendNow = true;
					Settings.EfaxModemDevice = "ttyS0";
					Settings.RefreshQueueInterval = 15;
					Settings.RefreshQueueEnabled = true;
				}
			} catch (Exception e) { 
				//TODO  HIG love required
				G_Message gm = new G_Message(
					Catalog.GetString(
@"Gconfd cannot find your settings. 
If you are running Gfax immediately 
after an installation, you may have 
to log out and log back in again."), TOPLEVEL);
				Environment.Exit(0);
			}

			// If we have a file name run the send dialog
			if (filename != null) {
				GfaxSend sd = new GfaxSend (filename, args);
				
				// send the faxes
				if (sd.DoSend) {
					fromSendWizard = true;
					
					// Start the fax daemon if efax
					if (Settings.TransmitAgent == "efax") {
						
						efax = new Efax();
						efax.run_efaxd();
					}

					Fax.sendfax(filename);
					// if file is temp data (/var/spool/gfax/D.*) then delete it
					FileInfo f = new FileInfo(filename);
					if (File.Exists(Defines.SPOOLDIR + f.Name))
						File.Delete(Defines.SPOOLDIR + f.Name);
					
					Gfax gf = new Gfax (filename, args);
				}
				
			}else {
			// check to see if we've run before, if so gfax will be there
				// Start the fax daemon if efax
				if (Settings.TransmitAgent == "efax") {
					efax = new Efax();
					efax.run_efaxd();
				}

				Gfax gf = new Gfax (filename, args);
			}
			
		}
	}
}
