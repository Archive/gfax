//  GFAX - Gnome fax application
//  Copyright (C) 2003 George A. Farris
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

		// Sequence to send a file is:
		// 	1) Make sure we're connected
		//	2) Store the filename on the server [storefile_open]
		//	3) Send the file line by line
		//	4) Close the stream for sending the file
		//	5) For all the phone numbers do:
		//		a) job_new				[job_new]
		//		b) set all job parms	[job_parm_set]
		//		c) submit the job		[submit_job]
		//	6) Close the connection.

// Properties
// 	string Hostname, Username, Password
//	int IPPort
namespace gfax {
	using System;
	using System.IO;
	using System.Text;			//Encoding.ASCII.GetBytes
	using System.Collections;	
	using System.Net;
	using System.Net.Sockets;


	public class MgettyQueue
	{
		public string Jobid;
		public string Number;
		public string Status;
		public string Owner;
		public string Pages;
		public string Dials;
		public string Error;
		public string Sendat;
	}
	
	/*
	CAUTION THIS IS NOT FUNCTIONAL AT ALL
	
	YOU HAVE BEEN WARNED.
	
	THERE IS NO MGETTY SUPPORT YET
	
	THIS FILE IS HERE AS A TEMPLATE ONLY
	
	*/
	
	public class Mgetty
	{	
		
		public bool connect ()
		{	
			return (true);
		}
		
		public void close ()
		{
			
		}

		public string status (string queue)
		{
			return ("mgetty");
						
		}

		//	send_init (string filename)
		//
		//	2) Store the filename on the server [storefile_open]
		//	3) Send the file line by line
		//	4) Close the stream for sending the file
		public string send_init (string fname)
		{
			return ("");
		}
		
		// Method send(string number)
		//
		// Sequence to send a file is:
		//		a) job_new				[job_new]
		//		b) set all job parms	[job_parm_set]
		//		c) submit the job		[submit_job]
		// format send time - should be as such:
		// yyyymmddhhmm
		//-> JPARM SENDTIME 200403100509
		//213 SENDTIME set to 20040310050900.
		public void send (string remote_fname, Contact contact)
		{
		}
		
	}
}
