//  GFAX - Gnome fax application
//  Copyright (C) 2003 George A. Farris
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

namespace gfax {
	using Mono.Posix;
	using System;
	using System.Collections;
	
	public class Fax
	{
		static Hylafax hfax;
		static Mgetty mfax;
		static Efax efax;
		static bool firstRun = true;

		public class FaxQueue
		{
			public string Jobid;
			public string Number;
			public string Status;
			public string Owner;
			public string Pages;
			public string Dials;
			public string Error;
			public string Sendat;
		}
		
		public class FaxRecQueue
		{
			public string Permissions;
			public string Pages;
			public string Status;			
			public string Owner;			
			public string Sender;
			public string TimeReceived;			
			public string Filename;			
		}
		
		public static string get_server_status ()
		{
			string status;
			
			if (Settings.TransmitAgent == "hylafax") {
				hfax = new Hylafax();
				
				if ( !hfax.connect() )
					return (Catalog.GetString("No Connection"));
				status = hfax.status("status");
				hfax.close();
				return (status);
			}
			
			if (Settings.TransmitAgent == "mgetty") {
					return ("Using Mgetty");
			}
			
			if (Settings.TransmitAgent == "efax") {
				//TODO put modem ready status here
				if (firstRun) {
					firstRun = false;					
					return (Catalog.GetString("Efax transmit process running...\nScanning job files every 60 seconds."));
				}
				else
					return (null);
			}
			
			return (Catalog.GetString("Error transport agent not specified!"));
		}

		public static ArrayList get_queue_status (string queue)
		{
			string reply = "";
			
			if (Settings.TransmitAgent == "hylafax") {
				hfax = new Hylafax();
				
				// this is here just to make sure the headers in the listview appear
				if ( hfax.connect() )
					reply = hfax.status(queue);
				hfax.close();
			}		
			
			//if (Settings.TransmitAgent == "mgetty") {
			//	list.Add("Using Mgetty");
			//}
			
			if (Settings.TransmitAgent == "efax") {
				reply = gfax.efax.status(queue);
			}
			
			if (queue == "doneq" || queue == "sendq")
				return parse_senddone(reply);
			else // (queue == "recvq")
				return parse_receive(reply);
			
		}
		
		// parse the send or done queue
		public static ArrayList parse_senddone (string reply)
		{
			ArrayList list = new ArrayList();
		
			if (reply.Length > 0) {
				foreach (string s in reply.Split('\n')) {
					FaxQueue hq = new FaxQueue();
					if (s.Length == 0 )
						break;
    	    		string[] sa = s.Split('=');
					if ( sa[0].Length != 0 )
						hq.Jobid = sa[0];
					else
						hq.Jobid = "";
					if ( sa[1].Length != 0 )
						hq.Number = sa[1];
					else
						hq.Number = "";	
					if ( sa[2].Length != 0 ) {
						switch (sa[2]) {
							case "R":
									hq.Status = Catalog.GetString("Run");
									break;
							case "S":
							        hq.Status = Catalog.GetString("Sleep");
							        break;
							case "B":
							        hq.Status = Catalog.GetString("Block");
							        break;
							case "W":
							        hq.Status = Catalog.GetString("Busy");
							        break;
							case "D":
							        hq.Status = Catalog.GetString("Done");
							        break;
							case "F":
							        hq.Status = Catalog.GetString("Fail");
							        break;
							case "P":
							        hq.Status = Catalog.GetString("New");
							        break;
						}
					}
					else
						hq.Status = "";
					if ( sa[3].Length != 0 )
						hq.Owner = sa[3];
					else
						hq.Owner = "";	
					if ( sa[4].Length != 0 )
						hq.Pages = sa[4];
					else
						hq.Pages = "";
					if ( sa[5].Length != 0 )
						hq.Dials = sa[5];
					else
						hq.Dials = "";	
					if ( sa[6].Length != 0 )
						hq.Error = sa[6];
					else
					hq.Error = "";
					
					if ( sa[7].Length != 0 ) { //2004/03/09 18.01.51
						string[] sa1 = sa[7].Split(' ');  //sa1[1] = 18.01.51
						string[] sa2 = sa1[1].Split('.'); //sa2[0] = 18
						string[] sa3 = sa1[0].Split('/'); //sa3[0] = 2004
						
						DateTime temp = new DateTime(
								Convert.ToInt32(sa3[0]), //Year
								Convert.ToInt32(sa3[1]), //Month
								Convert.ToInt32(sa3[2])  //Day
								);
						string dow = temp.DayOfWeek.ToString();
						hq.Sendat = "[" + dow.Substring(0,3) + "] " + sa2[0] + ":" + sa2[1];  //sa[7];
					} else {
						hq.Sendat = "";
					}
					
					list.Add(hq);
				}
				
			} else  {		// no status
			
				FaxQueue hq = new FaxQueue();
				hq.Jobid = "";
				hq.Number = "";
				hq.Status = "";
				hq.Owner = "";
				hq.Pages = "";
				hq.Dials = "";
				hq.Error = "";
				hq.Sendat = "";
				
				list.Add(hq);
			} 
        
			return (list);
		}

		// parse the receive queue
		//
		//Protect Page S  Owner        Sender/TSI  Recvd@ Filename
		//-rw-r--    1    10       +49 30 7865224 10Nov04 fax00004.tif
		// should come back as
		//-rw-r--=1= =10=+49 30 7865224=10Nov04=fax00004.tif
		public static ArrayList parse_receive (string reply)
		{
			ArrayList list = new ArrayList();
			
			if (reply.Length > 0) {
				foreach (string s in reply.Split('\n')) {
					FaxRecQueue hq = new FaxRecQueue();
					if (s.Length == 0 )
						break;
    	    		string[] sa = s.Split('=');
					if ( sa[0].Length != 0 )
						hq.Permissions = sa[0];
					else
						hq.Permissions = "";
					if ( sa[1].Length != 0 )
						hq.Pages = sa[1];
					else
						hq.Pages = "";	
					if ( sa[2].Length != 0 )
						switch (sa[2]) {
							case "*":
									hq.Status = Catalog.GetString("Receiving");
									break;
							case " ":
							        hq.Status = Catalog.GetString("Done");
							        break;
						}
					if ( sa[3].Length != 0 )
						hq.Owner = sa[3];
					else
						hq.Owner = "";	
					if ( sa[4].Length != 0 )
						hq.Sender = sa[4];
					else
						hq.Sender = "";
					if ( sa[5].Length != 0 )
						hq.TimeReceived = sa[5];
					else
						hq.TimeReceived = "";	
					if ( sa[6].Length != 0 )
						hq.Filename = sa[6];
					else
						hq.Filename = "";
					
					list.Add(hq);
				}
				
			} else  {		// no status
			
				FaxRecQueue hq = new FaxRecQueue();
				hq.Permissions = "";
				hq.Pages = "";
				hq.Status = "";
				hq.Owner = "";
				hq.Sender = "";
				hq.TimeReceived = "";
				hq.Filename = "";
				
				list.Add(hq);
			} 
        
			return (list);
		}

		
		
		public static void sendfax (string fname)
		{
			string remote_fname;
				
			if (Settings.TransmitAgent == "hylafax") {
				hfax = new Hylafax();
				// hylafax actually stores the file to the server
				hfax.connect();
				remote_fname = hfax.send_init(fname);
				
				// if "Cancel" button pressed on progess bar
				if (remote_fname == "cancelled") 
					return;
				
				#if DEBUGHYLAFAX
					Console.WriteLine("[Fax.sendfax] Remote file name is : {0}", remote_fname);
				#endif
				
				IEnumerator enu = gfax.Destinations.GetEnumerator();
    	  		while ( enu.MoveNext() ) {
    	     		// TODO try catch exception here
					
					hfax.send(remote_fname, (Contact)enu.Current);
				
					// open the log and log out going fax to server
					// Date Time PhoneNumber Organization ContactPerson
					// log file is in ~/.etc/gfax
					if (Settings.LogEnabled)
						log_it((Contact)enu.Current);
				}
			} 
			
			// Mgetty support
			if (Settings.TransmitAgent == "mgetty") {
				mfax = new Mgetty();
				mfax.connect();
				
				remote_fname = mfax.send_init(fname);
				if (remote_fname == "cancelled") 
					return;
				
				IEnumerator enu = gfax.Destinations.GetEnumerator();
    	  		while ( enu.MoveNext() ) {
					mfax.send(remote_fname, (Contact)enu.Current);
					if (Settings.LogEnabled)
						log_it((Contact)enu.Current);
				}

			}
			
			//Efax transport
			if (Settings.TransmitAgent == "efax") {
								
				// Convert the file with ghostscript
				string directory = gfax.efax.send_init(fname);
				if (directory == "cancelled") 
					return;
				
				IEnumerator enu = gfax.Destinations.GetEnumerator();
    	  		while ( enu.MoveNext() ) {
					gfax.efax.send(directory, (Contact)enu.Current);
					if (Settings.LogEnabled)
						log_it((Contact)enu.Current);
				}
			}
		}
		
		public static void log_it (Contact contact)
		{
		
		}
		
		public static void delete_job (string jobid)
		{
			if (Settings.TransmitAgent == "hylafax") {
				hfax = new Hylafax();
				hfax.connect();
				hfax.job_select(jobid);
				hfax.job_kill();
				hfax.job_delete();
			}
			
			if (Settings.TransmitAgent == "mgetty") {
				mfax = new Mgetty();
			}
			
			if (Settings.TransmitAgent == "efax") {
				gfax.efax.job_kill(jobid);
				gfax.efax.job_delete(jobid);
			}

		}
	}
	
}
