//  GFAX - Gnome fax application
//  Copyright (C) 2003 George A. Farris
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

namespace gfax {
	using Mono.Posix;
	using System;
	using System.IO;
	using System.Text;
	using System.Collections;
	
	// Phone book that holds multiple numbers
	public class Phonebook
	{
		public string Name;	//Name book is called by
		public string Path;	//Filename including path
		public string Type; // Type of phone book (gfax,gcard etc)
	}
	
	// Individual contact in phone book
	public class Contact
	{
		public string Organization;
		public string PhoneNumber;
		public string ContactPerson;
	}
	
	public class Phonetools
	{
		public static Phonebook[] get_phonebooks ()
		{
			StreamReader infile = null;
			string buf;
			string name, type, path;
			Phonebook[] pbooks = null;
						
			// TODO  get location from gconf
			string HOMEDIR = Environment.GetEnvironmentVariable("HOME");
			string PHONEBOOKS = HOMEDIR + "/.etc/gfax/phonebooks";
			
			#if DEBUG
				Console.WriteLine("[Phonebook] Reading phonebook file");
			#endif
			
			int numberOfBooks = get_number_of_books();
			
			//TODO check for empty file (numberOfBooks > 0)
			pbooks = new Phonebook[numberOfBooks];
			
			if ( !Directory.Exists(HOMEDIR + "/.etc/gfax")) {
				if ( !Directory.Exists(HOMEDIR + "/.etc")) {
					Directory.CreateDirectory(HOMEDIR + "/.etc");
				}
				Directory.CreateDirectory(HOMEDIR + "/.etc/gfax");
			}
				
			try { 
				infile = File.OpenText(PHONEBOOKS); 
				int i = 0;
				
				while ( (buf = infile.ReadLine()) != null ) {
					switch (buf.Trim()) {
						case "<book>" :
							// TODO more robust file reading
							Phonebook c = new Phonebook();
							c.Name = strip_tag(infile.ReadLine(), "name");
							c.Type = strip_tag(infile.ReadLine(), "type");
							c.Path = strip_tag(infile.ReadLine(), "path");
							pbooks[i++] = c;
							continue;
					}
				}

				infile.Close();
				return pbooks;			
			}
			catch (Exception e) { 
				return pbooks; 
			}

		}
		
		// save or create phonebooks files
		public static void save_phonebooks (Phonebook[] pbooks)
		{
			StreamWriter outfile;
			string buf;
			string name, type, path;
						
			// TODO  get location from gconf
			string HOMEDIR = Environment.GetEnvironmentVariable("HOME");
			string PHONEBOOKS = HOMEDIR + "/.etc/gfax/phonebooks";
			
			try { outfile = File.CreateText(PHONEBOOKS); }
			catch (Exception e) { return; }

			outfile.WriteLine("<gfax>");
			Console.WriteLine("Len :{0}", pbooks.Length);
			foreach (Phonebook p in pbooks) {
				outfile.WriteLine("	<book>");
				outfile.WriteLine("		<name>" + p.Name + "</name>");
				outfile.WriteLine("		<type>" + p.Type + "</type>");
				outfile.WriteLine("		<path>" + p.Path + "</path>");
				outfile.WriteLine("	</book>");
			}
			outfile.WriteLine("</gfax>");
			outfile.Close();
		}
		
		public static void delete_book (string book)
		{
			StreamReader infile = null;
			string buf;
			string deleteme = null;
			Phonebook[] pbooks = null;
			
			// make array size less 1 because we're deleting 1
			pbooks = new Phonebook[get_number_of_books() - 1];
			
			// TODO  get location from gconf
			string HOMEDIR = Environment.GetEnvironmentVariable("HOME");
			string PHONEBOOKS = HOMEDIR + "/.etc/gfax/phonebooks";
			
			try { 
				infile = File.OpenText(PHONEBOOKS); 
				int i = 0;
			
				// iterate through the phonebook file and skip past book to delete
				while ( (buf = infile.ReadLine()) != null ) {
					switch (buf.Trim()) {
						case "<book>" :
							// TODO more robust file reading
							Phonebook c = new Phonebook();
							c.Name = strip_tag(infile.ReadLine(), "name");
							c.Type = strip_tag(infile.ReadLine(), "type");
							c.Path = strip_tag(infile.ReadLine(), "path");
						
							if (c.Name != book)
								pbooks[i++] = c;
							else 
								deleteme = c.Path;
							continue;
					}
				}
				infile.Close();
				save_phonebooks(pbooks);
						
				if (File.Exists(deleteme))
					File.Delete(deleteme);
			}
			catch (Exception e) {
				return; 
			}
		}
		
		// Create or add the new phone book to the "phonebooks" file.
		public static void add_book (Phonebook p)
		{
			StreamReader infile = null;
			string buf;
			Phonebook[] pbooks = null;
			
			// make array plus 1 because we're adding 1
			pbooks = new Phonebook[get_number_of_books() + 1];
			
			// TODO  get location from gconf
			string HOMEDIR = Environment.GetEnvironmentVariable("HOME");
			string PHONEBOOKS = HOMEDIR + "/.etc/gfax/phonebooks";
			
			// add default path if not specified
			if (Path.GetDirectoryName(p.Path) == "")
				p.Path = HOMEDIR + "/.etc/gfax/" + p.Path ;
			
			// Create the file.
			if (!File.Exists(PHONEBOOKS)) {
           		FileStream fs = File.Create(PHONEBOOKS); 
				fs.Close();
			}

			int i = 0;
			try { 
				infile = File.OpenText(PHONEBOOKS); 
				
				while ( (buf = infile.ReadLine()) != null ) {
					switch (buf.Trim()) {
						case "<book>" :
							// TODO more robust file reading
							Phonebook c = new Phonebook();
							c.Name = strip_tag(infile.ReadLine(), "name");
							c.Type = strip_tag(infile.ReadLine(), "type");
							c.Path = strip_tag(infile.ReadLine(), "path");
							pbooks[i++] = c;
							continue;
					}
				}
				infile.Close();
			}
			catch (Exception e) { 
				// TODO catch file ops error
				return;
			}

			
			pbooks[i++] = p;	// add the new book
			save_phonebooks(pbooks);
		}

		// save a list (ArrayList) of contacts
		public static void save_phonebook_items (string book, ArrayList contacts)
		{
			Phonebook p;
			StreamWriter outfile;
			string buf;
			string name, type, path;
			
			p = get_book_from_name(book);
			
			// TODO error reporting
			try { outfile = File.CreateText(p.Path); }
			catch (Exception e) { return; }
			
			outfile.WriteLine("#Gfax phone book");
			
			IEnumerator enu = contacts.GetEnumerator();
    	  	while ( enu.MoveNext() ) {
				Contact c = new Contact();
				c = (Contact)enu.Current;
				outfile.WriteLine("{0}:{1}:{2}",c.PhoneNumber,c.ContactPerson,c.Organization);
			}
			outfile.Close();
		}
		
		public static StreamReader open_phonebook (Phonebook p)
		{
			StreamReader infile = null;
			
			try { 
				infile = File.OpenText(p.Path);
			}
			catch (Exception e) { 
				return null;
			}
			return infile;
		}
		
		public static ArrayList get_contacts (Phonebook p)
		{
			string buf = null;
			string[] sa;
			char[] ca = {':',':',':'};
			string name, number, company;
			ArrayList records = new ArrayList();			
			StreamReader fp = null;

			// TODO add popup message
			fp = open_phonebook(p);
			if (fp == null) {
				Console.WriteLine(Catalog.GetString("Can't open file : {0}"), p.Path);
				return null;
			}

			while ( (buf = fp.ReadLine()) != null ) {
				buf.Trim();
				
				if (buf[0] == '#')
					continue;
				else {
					sa = buf.Split(':');
					Contact contact = new Contact();
					contact.PhoneNumber = sa[0];
					contact.ContactPerson = sa[1];
					contact.Organization = sa[2];
					records.Add(contact);
				}
			}
			
			fp.Close();
			return records;
		}
		
		public static string strip_tag (string line, string tag)
		{
			string bt = "<" + tag + ">";
			string et = "</" + tag + ">";
			string s = (line.Trim()).Replace(bt, "");
			return s.Replace(et, "");
		}
		
		private static int get_number_of_books ()
		{
			StreamReader infile = null;
			string buf;
			int count = 0;
			
			// TODO  get location from gconf
			string HOMEDIR = Environment.GetEnvironmentVariable("HOME");
			string PHONEBOOKS = HOMEDIR + "/.etc/gfax/phonebooks";
			
			try { 
				infile = File.OpenText(PHONEBOOKS); 
			} catch (Exception e) { 
				return 0; 
			}
			
			// how many phone books do we have, Trim() removes white space
			while ( (buf = infile.ReadLine()) != null ) {
				if ( buf.Trim() == "<book>") {
					count++;
				}
			}
			infile.Close();
			return count;
		}
		
		public static Phonebook get_book_from_name(string name)
		{
			Phonebook[] books;
			int count;
			
			count = get_number_of_books();
			books = get_phonebooks();
			
			foreach (Phonebook p in books) {
				if (name == p.Name)
					return p;
			}
			return null;
		}
	}
}
