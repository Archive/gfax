//  GFAX - Gnome fax application
//  Copyright (C) 2003 George A. Farris
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Library General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

		// Sequence to send a file is:
		// 	1) Make sure we're connected
		//	2) Store the filename on the server [storefile_open]
		//	3) Send the file line by line
		//	4) Close the stream for sending the file
		//	5) For all the phone numbers do:
		//		a) job_new				[job_new]
		//		b) set all job parms	[job_parm_set]
		//		c) submit the job		[submit_job]
		//	6) Close the connection.

// Properties
// 	string Hostname, Username, Password
//	int IPPort
namespace gfax {
	using Mono.Posix;
	using System;
	using System.IO;
	using System.Text;			//Encoding.ASCII.GetBytes
	using System.Collections;	
	using System.Net;
	using System.Net.Sockets;

	public class Hylafax
	{	
		TcpClient mainclient = new TcpClient();
		NetworkStream mainstream;
		string host, user, pass;
		int port;
		
		// connect
		//		return false on sucess or true on cancel
		public bool connect ()
		{	
			host = Settings.Hostname;
			port = Convert.ToInt32(Settings.Port);
			user = Settings.Username;
			if (gfax.hylafaxPassword == null)	// if we already entered a pass but
				if (Settings.Password != "")	// don't want to remember it it won't be null
					gfax.hylafaxPassword = Settings.Password;
							
			#if DEBUGHYLAFAX
				Console.WriteLine ("Initializing hylafax class .......");
			#endif
			
			try {
		    	mainclient.Connect(host, port);
          		mainstream = mainclient.GetStream();
				string s = read(mainstream, mainclient);
				#if DEBUGHYLAFAX
					Console.WriteLine ("[hylafax.connect] on connect : {0}", s);
				#endif
				
						
		    	if(mainstream.CanWrite && mainstream.CanRead){
          	    	// Write username
					#if DEBUGHYLAFAX
						Console.WriteLine ("[hylafax.connect] Sending USER : {0}", user);
					#endif
					
					//If the username is null then hylafax doesn't need usernames
					// TODO check the hylafax protocol about this
					if (user == "")
						user = "anonymous";
					
					write (mainstream, "USER " + user + "\n");					
					// Read result
					// If the result is ("230 User <username> logged in.")
					// 		I don't need to send a password
					// Else if the result is ("331 Password required for <username>.")
					//		I must send a password.
          	    	string returndata = read(mainstream, mainclient);
					
					if (returndata.Substring(0,3) == "331") {
						//Console.WriteLine("Password is {0}", gfax.hylafaxPassword);
						for (int i = 0; i < 3; i++) {
							
							if (gfax.hylafaxPassword == null) {
								G_Password gpass = new G_Password();
								gpass.Run();
							
								if (gpass.Cancel) {
									return false;  // connection cancelled
								}
								gfax.hylafaxPassword = gpass.Password;
								if (gpass.RememberPassword)
									Settings.Password = gpass.Password;
								gpass.Close();
							}
							// 530 Login incorrect. result from bad password
							//prompt for password
							write (mainstream, "PASS " + gfax.hylafaxPassword + "\n");
							
							string rtn = read(mainstream, mainclient);
							//Console.WriteLine("Return is {0}", rtn);
							if (rtn.Substring(0,3) == "230")  // user is logged in
								break;
							else {
								gfax.hylafaxPassword = null;
								write (mainstream, "USER " + user + "\n");					
          	    				string rtndata = read(mainstream, mainclient);
							}
						}
					}
					
					#if DEBUGHYLAFAX
						Console.WriteLine("[hylafax.connect] USER returned : {0}", returndata);
					#endif
				}
				else if (!mainstream.CanRead) {
					Console.WriteLine(Catalog.GetString("You can not write data to this stream"));
					mainclient.Close();
				}
				else if (!mainstream.CanWrite) {             
					Console.WriteLine(Catalog.GetString("You can not read data from this stream"));
					mainclient.Close();
				}
				
				return true;
			}
			catch (Exception e ) {
				Console.WriteLine(e.ToString());
				G_Message m = new G_Message(Catalog.GetString("Could not connect to your Hylafax server.\n" +
					"Check console messages for further information\n\n" +
					"You might need to set a value for username."));
				return false;
			}
		}
		
		public void close ()
		{
			mainclient.Close();
		}

		// Method status(queue)
		//
		//	queue is the queue directory of the hylafax server it can be one of:
		//	'status', 'sendq', 'doneq' or 'recvq'
		//
		// Return a list containing lines	
		public string status (string queue)
		{
			#if DEBUGHYLAFAX
				Console.WriteLine ("[hylafax.status] queue is : {0}", queue);
			#endif
			// try 
			return getfolder(queue);
			
			//string[] s = String.split(getfolder(queue), '\015\012');
						
		}

		//	send_init (string filename)
		//
		//	2) Store the filename on the server [storefile_open]
		//	3) Send the file line by line
		//	4) Close the stream for sending the file
		public string send_init (string fname)
		{
			G_ProgressBar pbar;
			TcpClient myclient;
			NetworkStream mystream;
			StreamReader fp = null;
			string buf;
			string remote_fname;
			double lines = 0;
			double lines_sent = 0;
			double percent = 0;
			
			#if DEBUGHYLAFAX
				Console.WriteLine("[Hylafax.send_init] Filename : {0}", fname);
			#endif
			
			//timezone("LOCAL");
			// Setup the server and return a passive connection
			// PASV, STOT
			myclient = storefile_open();
			#if DEBUGHYLAFAX
				Console.WriteLine("[Hylafax.send_init] Got tcpclient");
			#endif
			
			mystream = myclient.GetStream();
			#if DEBUGHYLAFAX
				Console.WriteLine("[Hylafax.send_init] Stream is open");
			#endif
			//figure out how many lines in the file for progress bar
			// TODO progress bar and error 
			try { fp = File.OpenText(fname); }
			catch (Exception e) {  }
			
			while ( (buf = fp.ReadLine()) != null ) {
				lines = lines + 1;
			}
			fp.Close();
			
			try { fp = File.OpenText(fname); }
			catch (Exception e) {  }
			
			#if DEBUGHYLAFAX
				Console.WriteLine("[Hylafax.send_init] File :{0} is open and has {1} lines", fname,lines);
			#endif
			
			pbar = new G_ProgressBar();
			//pbar.Text = "File xmit";
			lines_sent = 1;
			while ( (buf = fp.ReadLine()) != null ) {
				write(mystream, buf);
				write(mystream, "\n");
				lines_sent = lines_sent + 1;
				percent = lines_sent / lines;
				if (percent <= 1.0)
					pbar.Fraction = percent;
				//GLib.MainContext.Iteration ();
				while (Gtk.Application.EventsPending ())
                	Gtk.Application.RunIteration ();
				if (pbar.Cancel)
					break;
			}
			fp.Close();
			remote_fname = storefile_close(myclient);
			
			if (pbar.Cancel) {	//If the transfer was cancelled
				deletefile(remote_fname);
				remote_fname = "cancelled";
				pbar.Close();
			} else {
				pbar.Finished();
			}
			
			#if DEBUGHYLAFAX
				Console.WriteLine("[Hylafax.send_init] Stream is closed, sent {0} lines",lines_sent);
			#endif
			return remote_fname;
		}
		
		// Method send(string filename_on_server, Contact contact)
		//
		// Sequence to send a file is:
		//		a) job_new				[job_new]
		//		b) set all job parms	[job_parm_set]
		//		c) submit the job		[submit_job]
		// format send time - should be as such:
		// yyyymmddhhmm
		//-> JPARM SENDTIME 200403100509
		//213 SENDTIME set to 20040310050900.
		public void send (string remote_fname, Contact contact)
		{
			string emailAddress = Settings.EmailAddress;
			string resolution = "98";
			string emailNotify = "none";
			
			#if DEBUGHYLAFAX
				Console.WriteLine("Hylafax.send] gfax.timeToSend : {0}", gfax.timeToSend);
				Console.WriteLine("[Hylafax.send] Remote file name is : {0}", remote_fname);
			#endif
			
			// if this is sent from GfaxSend wizard
			if (gfax.fromSendWizard) {			
				if (gfax.sendWizardResolution)
					resolution = "196";
			
				if (gfax.sendWizardEmailNotify) {
					emailNotify = "done";
					emailAddress = gfax.sendWizardEmailAddress;
				}
			} else {
				if (Settings.EmailNotify)
					emailNotify = "done";
					
				if (Settings.HiResolution)
					resolution = "196";
			}
			
			// Format time to send - timezone is in UTC format.
			string tts = String.Format("{0}{1:00}{2:00}{3:00}{4:00}",
					gfax.timeToSend.Year,
					gfax.timeToSend.Month,
					gfax.timeToSend.Day,
					gfax.timeToSend.Hour,
					gfax.timeToSend.Minute);
			
			string jid = job_new();
			//TODO try catch exception here
			//#item[1] is the name	#item[2] is the company 
			
			job_param_set("FROMUSER", Environment.UserName);
			job_param_set("LASTTIME", "000259");
			job_param_set("SENDTIME", tts);
			job_param_set("MAXDIALS", "12");
			job_param_set("MAXTRIES", "3");	
			job_param_set("SCHEDPRI", "127");		
			job_param_set("DIALSTRING", contact.PhoneNumber);
			job_param_set("NOTIFYADDR", emailAddress);
			job_param_set("VRES", resolution);		
			job_param_set("PAGEWIDTH", "215");
			job_param_set("PAGELENGTH", "279");
			job_param_set("NOTIFY", emailNotify);	//can be "none" or "done"
			job_param_set("PAGECHOP", "default");
			job_param_set("CHOPTHRESHOLD", "3");
			job_param_set("DOCUMENT", remote_fname);
			job_submit();
		}
		
		private string job_new ()
		{
			string data;
			string[] tmp;
			//string jobid;
			
			write(mainstream, "JOB default\n");
			data = read(mainstream, mainclient);
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.job_new] \"JOB default\" data returned : {0}", data);
			#endif

			write(mainstream, "JNEW\n");
			data = read(mainstream, mainclient);
			// data = 200 New job created: jobid: 14433 groupid: 14433.
			tmp = data.Split(' ');

			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.job_new] \"JNEW\" data returned : {0}", data);
			#endif
			
			// jobid might need to be Convert.ToInt32
			return tmp[5];
		}
		
		private void job_param_set (string pname, string pvalue)
		{
			string data;
		
			write(mainstream, "JPARM " + pname + " " + pvalue +"\n");
			data = read(mainstream, mainclient);	
			#if DEBUGHYLAFAX
				Console.Write("[hylafax.job_param_set] \"JPARM\" data returned : {0}", data);
			#endif
		}

		private void job_submit ()
		{
			string data;
			
			write(mainstream, "JSUBM\n");
			data = read(mainstream, mainclient);
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.job_submit] \"JSUBM\" data returned : {0}", data);
			#endif
		}
		
		private TcpClient storefile_open ()
		{
			TcpClient myclient = new TcpClient();
			NetworkStream mystream;
			IPAddress ipaddr;
			int ipport;
			string data;
			
			write(mainstream, "PASV\n");	
			data = read(mainstream, mainclient);

			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.storefile_open] data returned : {0}", data);
			#endif
			
			// dig out ip address and port for new connection
			ipaddr = get_ip_addr(data);
			ipport = get_ip_port(data);
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.storefile_open] ipaddr, port : {0}, {1}", ipaddr, ipport);
			#endif
			
			try {
				myclient.Connect(ipaddr, ipport);
			} 
		    catch (Exception e ) {
				Console.WriteLine(e.ToString());
				// handle error here
			}
			
			mystream = myclient.GetStream();			
			write(mainstream, "STOT\n");
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.storefile_open] wrote STOT");
			#endif
			data = read(mainstream, mainclient);
			//data = 150 FILE: /tmp/doc1097.ps (Opening new data connection).
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.storefile_open] returned from STOT : {0}", data);
			#endif
			return myclient;
		}

		private string storefile_close(TcpClient tc)
		{
			string data;
			string[] s;
			string fn;
						
			tc.Close();
			data = read(mainstream, mainclient);
			// data = 226 Transfer complete (FILE: /tmp/doc1101.ps).
			// TODO error checking
			
			// remove white space first, Arrgggh!
			fn = data.Trim();
			s = fn.Split(' ');

			return(s[4].TrimEnd(')','.'));
		}
		
		private string getfolder (string folder)
		{
			TcpClient myclient = new TcpClient();
			NetworkStream mystream;
			IPAddress ipaddr;
			int ipport;
			string jobfmt;
			
			write(mainstream, "PASV\n");	
			string data = read(mainstream, mainclient);
			
			// dig out ip address and port for new connection
			ipaddr = get_ip_addr(data);
			ipport = get_ip_port(data);

			try {
				myclient.Connect(ipaddr, ipport);
			} 
		    catch (Exception e ) {
				Console.WriteLine(e.ToString());
				// handle error here
			}
			
			mystream = myclient.GetStream();			
			/*
			RCVFMT (receive)
			%-7m %4p%1z %-8.8o %14.14s %7t %f
			m - file perms
			p - number of pages
			z - * if still receiving, <space> otherwise
			o - file owner
			s - sender (TIS)
			t - time received
			f - filename
			
			JOBFMT (send, done)
			j - Job identifier
			e - Phone number
			a - Job state (one-character symbol)
			o - Job owner
			P - # pages transmitted/total # pages to transmit
			D - Total # dials/maximum # dials
			s - Job status information from last failure
			Y - Scheduled date and time
			*/
			// set the job format string
			if (folder == "doneq" || folder == "sendq") {
				jobfmt = "\"%-4j=%-14e=%1a=%-12o=%-5P=%-5D=%.35s=%-19Y\"";
				write(mainstream, "JOBFMT "+jobfmt+"\n");
				data = read(mainstream, mainclient);
			}
			if (folder == "recvq") {
				jobfmt = "\"%-7m=%4p=%1z=%-8.8o=%14.14s=%7t=%f\"";
				write(mainstream, "RCVFMT "+jobfmt+"\n");
				data = read(mainstream, mainclient);
			}
			write(mainstream, "LIST "+folder+"\n");
			data = read(mystream, myclient);
							
						
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.getfolder] list folder data : {0}", data);
				Console.WriteLine("[hylafax.getfolder] data length : {0}", data.Length);				
			#endif

			myclient.Close();
			return data;
		}

		private IPAddress get_ip_addr (string ipdata)
		{
			int index = ipdata.IndexOf("(");
			int len = ipdata.IndexOf(")") - index;
			string s = ipdata.Substring(index + 1, len - 1 );
			char[] splitter  = {','};
			string[] sa = s.Split(splitter);
			string sipaddr = sa[0]+"."+sa[1]+"."+sa[2]+"."+sa[3];
			return IPAddress.Parse(sipaddr);
		}
		
		private int get_ip_port (string ipdata)
		{
			// find port returned
			int index = ipdata.IndexOf("(");
			int len = ipdata.IndexOf(")") - index;
			string s = ipdata.Substring(index + 1, len - 1 );
			char[] splitter  = {','};
			string[] sa = s.Split(splitter);
			return (Convert.ToInt32(sa[4]) * 256 + Convert.ToInt32(sa[5]));
		}

		
		private void write (NetworkStream sock, string s)
		{
			byte[] bytes = Encoding.ASCII.GetBytes(s);
			sock.Write(bytes, 0, bytes.Length);
		}

		
		private string read (NetworkStream str, TcpClient c)
		{
			StringBuilder buf = new StringBuilder();
			byte[] bytes = new byte[256];
			
			do {
				try {
					
					int len = str.Read(bytes, 0, bytes.Length);
					if (len > 0 ) {
						buf.Append(Encoding.ASCII.GetString(bytes,0,len));
					} 
				} catch ( Exception e ) {
					Console.WriteLine(e.ToString());
				}
				// Seems if we don't sleep here for a bit, we never read
				// all the data???  Maybe a Hylafax thing.
				System.Threading.Thread.Sleep(1);
					
			} while (str.DataAvailable);
			return (buf.ToString()); 
		}
		

		public void job_select (string jobid)
		{
			write(mainstream, "JOB " + jobid + "\n");
			string s = read(mainstream, mainclient);
		}
		
		public void job_kill ()
		{
			write (mainstream, "JKILL\n");
			string s = read(mainstream, mainclient);
		}
		
		public void job_delete ()
		{
			write (mainstream, "JDELE\n");
			string s = read(mainstream, mainclient);
		}

		private void job_param (string parms)
		{
			write (mainstream, "JPARM " + parms+"\n");
			string s = read(mainstream, mainclient);
		}

		private void deletefile (string name)
		{
			write (mainstream, "DELE " + name + "\n");
			string s = read(mainstream, mainclient);
		}
		
		private void timezone (string name)  // can be "LOCAL" or "GMT"
		{
			write (mainstream, "TZONE " + name + "\n");
			string s = read(mainstream, mainclient);
			#if DEBUGHYLAFAX
				Console.WriteLine("[hylafax.timezone] returned: {0}", s);
			#endif
		}
	}
}
